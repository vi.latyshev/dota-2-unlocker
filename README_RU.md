# DotA 2 Unlocker

* [Instruction in English](../master/README.md)

### Патчинг бинарных файлов DotA 2

* Автоопределение пути DotA 2
* Патчинг:

| patch / os bit              | x32   | x64   |
|-----------------------------|:-----:|:-----:|
| [sv_cheats](#sv_cheats)     | **+** | **+** |


## Установка

* Установить [.NET Framework 4.5](https://www.microsoft.com/en-us/download/details.aspx?id=30653)
* Скачать [Dota2Unlocker.zip](../../releases) из последнего релиза

## Использование

* Запустите `Dota2Unlocker.exe`
	* Если путь не определился автоматически, то введите его вручную (включая 'dota 2 beta')
* Подождать пока DotA 2 файлы будут пропатчены (будет написано `Patched`)
* Закрыть `Dota2Unlocker.exe`
* Done ;)

## **ВНИМАНИЕ: Патчить нужно почти после каждого обновления клиента игры!**

## Удаление патчинга из DotA 2

* Просто [Проверьте целостность файлов игры](https://support.steampowered.com/kb/2037-QEUH-3335/verify-integrity-of-game-cache?l=russian)
* Все пропатченные файлы будут удалены

## sv_cheats

* Только для клиента игры (client side)!

### Популярные конвары (convars) (cheat commands):

* `sv_cheats`: по-умолчанию `0` (after patching `1`)
	* `1` - Разблокировка конваров (cheat commands) с флагом `cheat`
		* ex. `sv_cheats 1`
* `dota_use_particle_fow`: по-умолчанию `1`
	* `0` - Показ скрытых способностей (партиклей) и телепортов в тумане войны. 
		* ex. `dota_use_particle_fow 0`
* `fog_enable`: по-умолчанию `1`
	* `0` - Убирает дымку/туман 
		* ex. `fog_enable 0`
* `fow_client_nofiltering`: по-умолчанию `0`
	* `1` - Убирает небольшую дымку для четкости видимости 
		* ex. `fow_client_nofiltering 1`
* `dota_camera_distance`: по-умолчанию `1134`
	* `*any number*` - изменение дистанции камеры
		* ex. `dota_camera_distance 1600`
* `r_farz`: по-умолчанию `-1`
	* `18000` - Убирает полоску сверху экрана при отдалении
	* Вам нужно умножать на x2 от дистанции камеры или просто установите `18000`
		* ex. `dota_camera_distance 1600` -> `r_farz 3200`
* `dota_range_display`: по-умолчанию `0`
	* `*any number*` - Отображает круг вокруг локального героя
		* ex. `dota_range_display 1200`
* `cl_weather`: по-умолчанию `0`
	* `*any number*`(1-10) - Изменение погоды
		* ex. `cl_weather 8`
	* Весь список:
		1. "по-умолчанию"
		2. "Snow"
		3. "Rain"
		4. "Moonbeam"
		5. "Pestilence"
		6. "Harvest"
		7. "Sirocco"
		8. "Spring"
		9. "Ash"
		10. "Aurora"

### Raw

* просто скопируйте этот список и вставьте в консоль после патчинга

```
sv_cheats 1;
dota_use_particle_fow 0;
fog_enable 0;
fow_client_nofiltering 1;
dota_camera_distance 1600;
r_farz 18000;
dota_range_display 1200;
cl_weather 8;
```

### Весь список

* [Весь список консольных комманд](https://dota2.gamepedia.com/List_of_Console_Commands)