﻿using System;

namespace Dota2Unlocker
{
    class Program
    {
        static void Main()
        {

            Paths.DetectOSbit();

            if (!Paths.AutoFindDotaPath())
            {
                EnterDotaPath();
            }

            Patcher.Patching();

            if (Patcher.WithErrors)
            {
                Console.WriteLine("Not patched");
            }
            else
            {
                Console.WriteLine("Patched");
            }

            Console.ReadKey();
        }

        static void EnterDotaPath()
        {
            Console.WriteLine("Enter the path to DotA folder (include 'dota 2 beta')");

            if (!Paths.ManuallyDotaPath(Console.ReadLine()))
            {
                EnterDotaPath();
            }
        }
    }
}
