﻿using System;
using System.IO;

using Gameloop.Vdf;
using Microsoft.Win32;
using Gameloop.Vdf.Linq;
using Gameloop.Vdf.JsonConverter;

namespace Dota2Unlocker
{
    static class Paths
    {

        private static readonly string RegeditPath = @"HKEY_CURRENT_USER\Software\Valve\Steam";
        private static readonly string FindKeyInPath = "SteamPath";

        private static string PathToLibraryFolder(string steamPath) => Path.Combine(steamPath, "steamapps", "libraryfolders.vdf");

        private static string PathCombine(string path) => Path.Combine(path, "steamapps", "common", "dota 2 beta");

        private static string PathEngine(string sysTypeBit) => Path.Combine("game", "bin", sysTypeBit, "engine2.dll");

        private static bool CheckDotaPath(string path, string sysTypeBit = null) =>
            sysTypeBit != null
            && File.Exists(Path.Combine(path, "game", "bin", sysTypeBit, "dota2.exe"))
            && File.Exists(Path.Combine(path, "game", "bin", sysTypeBit, "engine2.dll"));


        public static string PathToEngine(string sysTypeBit) => Path.Combine(DotaPath, PathEngine(sysTypeBit));

        public static bool Is64Bit { get; private set; } = false;

        public static string DotaPath { get; private set; } = null;

        
        public static void DetectOSbit()
        {
            Is64Bit = Environment.Is64BitOperatingSystem;

            if (Is64Bit)
            {
                Console.WriteLine("Detected x64bit. Patching for x32 and x64 version of Dota 2");
            }
            else
            {
                Console.WriteLine("Detected x32bit. Patching only for x32 version of Dota 2");
            }
        }

        public static bool AutoFindDotaPath()
        {
            string steamPath = (string)Registry.GetValue(RegeditPath, FindKeyInPath, null);

            if (steamPath == null)
            {
                Console.WriteLine("[Auto Find] Steam Path not found");
                return false;
            }

            string dotaPath = PathCombine(steamPath);

            if (CheckDotaPath(dotaPath))
            {
                DotaPath = dotaPath;
                return true;
            }

            string folders = File.ReadAllText(PathToLibraryFolder(steamPath));

            VProperty libraryFolders = VdfConvert.Deserialize(folders);

            dynamic libraryFoldersJSON = libraryFolders.Value.ToJson();

            foreach (var name in libraryFoldersJSON)
            {
                if (name.Name == "TimeNextStatsReport" || name.Name == "ContentStatsID")
                    continue;

                steamPath = name.Value.Value;

                dotaPath = PathCombine(steamPath);

                if (ManuallyDotaPath(dotaPath))
                {
                    return true;
                }
            }

            Console.WriteLine("[Auto Find] Dota 2 Path not found");
            return false;
        }

        public static bool ManuallyDotaPath(string path)
        {
            bool checkPath = CheckDotaPath(path, "win32");

            if (Is64Bit)
                checkPath &= CheckDotaPath(path, "win64");

            if (checkPath)
            {
                DotaPath = path;
            }

            return checkPath;
        }
    }
}
