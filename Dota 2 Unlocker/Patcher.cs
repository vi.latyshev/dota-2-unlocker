﻿using System;
using System.IO;

namespace Dota2Unlocker
{
    static class Patcher
    {
        public static bool WithErrors { get; private set; } = false;

        //--------------------------------------------engine2.dll x32-------------------------------------------------------------------------------

        private static readonly byte[][][] sigEngine32 = {

            // SV: Convar '%s' is cheat protected,
            new byte[2][] {

                // original
                new byte[] { 0x68, 0x00, 0x40, 0x00, 0x00, 0xFF, 0xD0, 0x84, 0xC0, 0x74 },

                // cracked
                new byte[] { 0x68, 0x00, 0x40, 0x00, 0x00, 0xFF, 0xD0, 0x84, 0xC0, 0xEB }
            },
        
            // can't change replicated convar
            new byte[2][] {

                // original
                new byte[] {0x68, 0x00, 0x20, 0x00, 0x00, 0x8B, 0x40, 0x0C, 0xFF, 0xD0, 0x84, 0xC0, 0x74, 0xCC, 0x8B, 0x0D, 0xCC, 0xCC, 0xCC, 0xCC, 0x8B, 0x01 },

                // cracked
                new byte[] { 0x68, 0x00, 0x20, 0x00, 0x00, 0x8B, 0x40, 0x0C, 0xFF, 0xD0, 0x84, 0xC0, 0xEB }
            }
        };

        //--------------------------------------------engine2.dll x64-------------------------------------------------------------------------------

        private static readonly byte[][][] sigEngine64 = {

            // SV: Convar '%s' is cheat protected,
            new byte[2][] {

                // original
                new byte[] { 0x74, 0xCC, 0xE8, 0xCC, 0xCC, 0xCC, 0xCC, 0x84, 0xC0, 0x75, 0xCC, 0x48, 0x8B, 0x05, 0xCC, 0xCC, 0xCC, 0xCC, 0x83, 0x78, 0x58, 0x00, 0x74, 0xCC, 0xE8 },

                // cracked
                new byte[] { 0xEB }
            },
        
            // can't change replicated convar
            new byte[2][] {

                // original
                new byte[] { 0xFF, 0x50, 0x18, 0x84, 0xC0, 0x74, 0xCC, 0x48, 0x8B, 0x0D, 0xCC, 0xCC, 0xCC, 0xCC, 0x48, 0x8B, 0x01, 0xFF, 0x90, 0x10, 0x01, 0x00, 0x00 },

                // cracked
                new byte[] { 0xFF, 0x50, 0x18, 0x84, 0xC0, 0xEB }
            }
        };

        //-----------------------------------------------------------------------------------------------------------------------------------------

        private static int FindBytes(byte[] src, byte[] find)
        {
            int index = -1;
            int matchIndex = 0;

            for (int i = 0; i < src.Length; i++)
            {
                if (find[matchIndex] == 0xCC || src[i] == find[matchIndex])
                {
                    if (matchIndex == (find.Length - 1))
                    {
                        index = i - matchIndex;
                        break;
                    }
                    matchIndex++;
                }
                else if (src[i] == find[0])
                {
                    matchIndex = 1;
                }
                else
                {
                    matchIndex = 0;
                }

            }
            return index;
        }

        private static byte[] ReplaceBytes(byte[] src, byte[] search, byte[] repl)
        {
            byte[] dst = null;
            int index = FindBytes(src, search);
            if (index >= 0)
            {
                dst = new byte[src.Length];
                Buffer.BlockCopy(src, 0, dst, 0, index);
                Buffer.BlockCopy(repl, 0, dst, index, repl.Length);
                Buffer.BlockCopy(
                    src,
                    index + repl.Length,
                    dst,
                    index + repl.Length,
                    src.Length - (index + repl.Length));
            }
            return dst;
        }


        private static void PatchByBit(string pathBit, byte[][][] sigsEngine)
        {
            string pathToEngine = Paths.PathToEngine(pathBit);

            try
            {
                byte[] engineBytes = File.ReadAllBytes(pathToEngine);

                foreach (byte[][] find in sigsEngine)
                {
                    if (engineBytes != null)
                        engineBytes = ReplaceBytes(engineBytes, /* original */ find[0], /* cracked */ find[1]);
                }

                if (engineBytes != null)
                    File.WriteAllBytes(pathToEngine, engineBytes);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Patching] Error {0}:\n{1}", pathBit, ex.Message);
                WithErrors = true;
            }
        }

        public static void Patching()
        {
            PatchByBit("win32", sigEngine32);

            if (!Paths.Is64Bit)
                return;

            PatchByBit("win64", sigEngine64);
        }
    }
}
